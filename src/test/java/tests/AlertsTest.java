package tests;

import pages.AlertsPage;
import org.testng.annotations.Test;

public class AlertsTest extends BaseTest {
	
	@Test
	public void simpleAlertTest()
		{
			AlertsPage page = new AlertsPage(driver);
			page.goToPane().getButton(page.SimpleAlertButton).click();
			page.getAlertTextandAccept();
		}
	
}


