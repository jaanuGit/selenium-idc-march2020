package testng;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;

public class NewTest {
	
	WebDriver driver;
	
  @Test
  public void first() {
	  System.out.println("The title is :"+driver.getTitle());
  }
  
  @Test
  public void second() {
	  WebElement box = driver.findElement(By.name("searchBox"));
	  System.out.println("Element box is:"+box);
  }
  
  @Test(enabled = false)
  public void third() {
	  System.out.println("Skipped");
  }
  
  @Test
  public void fourth() throws SkipException {
	  throw new SkipException("This method will be skipped");
  }  
  
  
  @BeforeTest
  public void beforeTest() {
	  driver = new FirefoxDriver();
	  driver.get("https://training-support.net");
  }

  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
