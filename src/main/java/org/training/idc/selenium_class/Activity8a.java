package org.training.idc.selenium_class;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.*;

public class Activity8a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver,30);  //good practice to initialize here soon after the driver.
		
		driver.get("https://www.training-support.net/selenium/dynamic-controls");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		WebElement ele = driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input"));
		boolean status = ele.isDisplayed();
		System.out.println("The checkbox is : "+ status);
		
		driver.findElement(By.id("toggleCheckbox")).click();

		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input")));
		
			
		//WebDriverWait wait = new WebDriverWait(driver,30);
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input")));

		driver.close();
		
	}

}
