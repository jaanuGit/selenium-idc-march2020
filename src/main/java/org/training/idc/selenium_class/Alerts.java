package org.training.idc.selenium_class;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class Alerts {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30,TimeUnit.SECONDS);
		
		driver.get("https://www.training-support.net/selenium/javascript-alerts");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		//Click the button to open a simple alert
		driver.findElement(By.cssSelector("button#simple")).click();
		
		// Switch to alert window
		Alert simpleAlert = driver.switchTo().alert();
		
		// Get text in the alert boc and print it
		String alertText = simpleAlert.getText();
		System.out.println("Alert text is : "+alertText);
		
		//close the alert box
		// The sleep is not recommended
		// to see the alert
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		simpleAlert.accept();
		
		driver.close();
		

	}

}
