package org.training.idc.selenium_class;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import java.util.concurrent.TimeUnit;

public class Activity8b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WebDriver driver = new FirefoxDriver();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30,TimeUnit.SECONDS);
		
		driver.get("https://www.training-support.net/selenium/ajax");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/button")).click();
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/div[2]/h1")));
		//System.out.println("Page Content message : "+driver.findElement(By.xpath(("/html/body/div[2]/div/div[2]/h1"))).getText());
		
		wait.until(ExpectedConditions.textToBePresentInElementLocated(By.id("ajax-content"),"HELLO"));
		System.out.println("Page Content message : ");
		
	}

}
