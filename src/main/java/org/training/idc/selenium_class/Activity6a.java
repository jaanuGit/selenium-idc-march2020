package org.training.idc.selenium_class;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity6a {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//List<Integer> jArray2 = new ArrayList<>;
		System.out.println("Activity for reading from the table");
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/tables");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		System.out.println("No.of rows in the table :" +driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr")).size());
		System.out.println("No.of columns in the table :" +driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[1]/td")).size());
		
		//WebElement tablerow =  (WebElement) driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[2]"));
		WebElement tabledata = driver.findElement(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr[2]/td[1]"));
		
		//ArrayList<Object> tableRow = new ArrayList<>();
		//tableRow = driver.findElements(By.xpath("/html/body/div/div[2]/div/div[2]/div[2]/table/tbody/tr")).size();
		String rowText = tabledata.getText();
		System.out.println(rowText);
		
		
		
	//	System.out.println("The cell value for third row is : " + jArray2.size());
		driver.close();
		

	}

}
