package org.training.idc.selenium_class;

import java.awt.List;
import java.util.ArrayList;

public class collActivity1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Collections Activity for ArrayList - Ragas");
		
		ArrayList<String> myList = new ArrayList<>();
		myList.add("Sankarabharanam");
		myList.add("Natai");
		myList.add("Kalyani");
		myList.add("Hindolam");
		myList.add("Saranga");
			
			
		for (int j=0; j< myList.size();j++)
		{
			System.out.println("Raga is :"+myList.get(j));
		}
		
		System.out.println("Get the third element from ArrayList");
		String jThird = myList.get(2);
		System.out.println("Element is :"+jThird);
		
		String jExists = "Abogi";
		
		for (int k=0; k < myList.size();k++)
		{
			
			System.out.println("Raga is :"+myList.get(k));
		}
		
	}

}
