package org.training.idc.selenium_class;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AmazonCart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver,30);  
		driver.manage().window().maximize();
		
		System.out.println("Opening Amazon website");
		driver.get("https://www.amazon.in/");
		
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Samsung");
		//driver.findElement(By.id("twotabsearchtextbox")).sendKeys(Keys.ENTER);
		
		//List<WebElement> ele = driver.findElements(By.id("suggestions"));
		//System.out.println("Total no.of suggestions :"+ele.size());
		
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-keyword='samsung headphones original']")));
		element.click();
		
		driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div/span[4]/div[1]/div[5]/div/span/div/div/div[2]/h2")).click();
		boolean ele2 = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div[2]/div/span[4]/div[1]/div[5]/div/span/div/div/div[2]/h2")).isSelected();
		System.out.println("Item is selected :"+ele2);
		
	
		
						
	}

}
