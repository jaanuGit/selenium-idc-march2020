package org.training.idc.selenium_class;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity3 {

	public static void main(String[] args) {
				
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/simple-form");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		driver.findElement(By.id("firstName")).sendKeys("Janani");
		driver.findElement(By.id("lastName")).sendKeys("Sriram");
		driver.findElement(By.id("email")).sendKeys("janani.iyer@oracle.com");
		driver.findElement(By.id("number")).sendKeys("9440730556");
			
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div/div[5]/textarea")).sendKeys("Hello!!! Im SJ");
		
		driver.findElement(By.xpath("/html/body/div[2]/div/div/div/div[2]/form/div/div[6]/div[1]/input")).click();
		//driver.close();
		

	}

}
