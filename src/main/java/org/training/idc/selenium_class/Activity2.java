package org.training.idc.selenium_class;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net");
		driver.findElement(By.tagName("a")).click();
		System.out.println("Get the page Title");
		
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
					
		driver.close();
		
	}

}
