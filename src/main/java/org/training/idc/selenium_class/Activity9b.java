package org.training.idc.selenium_class;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class Activity9b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriver driver = new FirefoxDriver();
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(30,TimeUnit.SECONDS);
		
		driver.get("https://www.training-support.net/selenium/drag-drop");
		
		WebElement ball = driver.findElement(By.xpath("//*[@id=\"draggable\"]"));
		WebElement drop1 = driver.findElement(By.id("droppable"));
		WebElement drop2 = driver.findElement(By.id("dropzone2"));
		
		Actions customAction = new Actions(driver);
		//Action myAction = customAction.moveToElement(ball);
		customAction.dragAndDrop(ball, drop1).build().perform();
		System.out.println("The ball is :"+driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/p")).getText());
		
		customAction.dragAndDrop(ball, drop2).build().perform();
		System.out.println("The ball is :"+driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/p")).getText());
		
		
		

	}

}
