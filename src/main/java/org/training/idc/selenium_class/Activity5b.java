package org.training.idc.selenium_class;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity5b {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/dynamic-controls");
		
		System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);
		
		WebElement ele = driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input"));
		boolean status = ele.isSelected();
		System.out.println("The checkbox is : "+ status);
		driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input")).click();
		
		WebElement ele2 = driver.findElement(By.xpath("/html/body/div[3]/div/div[1]/div[1]/input"));
		boolean status2 = ele2.isSelected();
		System.out.println("The checkbox is : "+ status2);
		
			}

}
