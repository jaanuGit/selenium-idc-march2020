package org.training.idc.selenium_class;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BMTestNavigation {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		WebDriver driver = new FirefoxDriver();
		WebDriverWait wait = new WebDriverWait(driver,30);
				
		System.out.println("Opening Fusion Applications");
		driver.get("https://fuscdrmsmc126-fa-ext.us.oracle.com/homePage/faces/FuseWelcome");
		
		//System.out.println("Get the page Title");
		String jtitle = driver.getTitle();
		System.out.println("The title of the page is : "+ jtitle);

		// Enter user name
		driver.findElement(By.xpath("//*[@id=\"userid\"]")).sendKeys("aps_all");
		//Enter password
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("Welcome1");
		//Click on Login button
		driver.findElement(By.xpath("//*[@id=\"btnActive\"]")).click();
		Thread.sleep(9000);
		
		System.out.println("Successfully Logged in to the application");
		
		//Navigate to Backlog Management Work-Area
		driver.findElement(By.cssSelector("#pt1\\3A_UISmmLink\\3A\\3Aicon")).click();
		Thread.sleep(2000);

		driver.findElement(By.xpath("//table[@class='NavigatorContainer af_panelGroupLayout']/descendant::a[text()='Backlog Management']")).click();
		//driver.findElement(By.xpath("//*[@id=\"pt1:nv_itemNode_SupplyChainPlanning_BacklogManagement\"]")).click();
		
		driver.findElement(By.cssSelector("#_FOpt1\\:_FOr1\\:0\\:_FOSritemNode_SupplyChainPlanning_BacklogManagement\\:0\\:_FOTsdiworkbenchRegionalTaskList\\:\\:icon")).click();;
		System.out.println("You are in Tasks pane");
		
		Thread.sleep(2000);
		// Create new Demand Priority Rule
		driver.findElement(By.xpath("//div[@class='af_panelList']/ul/li[9]/a")).click();
		System.out.println("You have selected 'Manage Demand Priority Rules'");
		
		driver.findElement(By.xpath("//td/div/a/img")).click();
		
		
	}

}
